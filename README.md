Introduction
------------




Files
-----

[conftest.py](conftest.py) contains fixtures for tests.

[pages/](pages) contains PageObject pattern implementation for Python.

[utils/api.py](utils) contains helper Api class for work with api.

[utils/postg_db.py](utils) contains helper DataBase class for work with PostgreDB.

[pages/elements.py](pages/elements.py) contains helper class to define web elements on web pages.

[tests/](tests) contains several Web UI,API and DB tests for http://localhost:8000/

[utils/simple_config.ini](utils) config with all the data that was used in the tests

[utils/config_reader.py](utils) contains helper class for reading config file [utils/simple_config.ini](utils)



How To Run Tests
----------------

1) Install all requirements:

    ```bash
    pip3 install -r requirements
    ```


2) Run tests:

   For running TC_1
    ```bash
    pytest -q --browser=chrome -v -m tc_1 
    ```
   For running TC_2
    ```bash
    pytest -q --browser=chrome -v -m tc_2
    ```
   For running TC_3
    ```bash
    pytest -q --browser=chrome -v -m tc_3 
    ```
   For running TC_4
    ```bash
    pytest -q --browser=chrome -v -m tc_4 
    ```
   For running first api TC
    ```bash
    pytest -v -m user 
    ```
   For running second api TC
    ```bash
    pytest -v -m pet 
    ```
   

![alt text](example.png)

