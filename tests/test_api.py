#!/usr/bin/python3
# -*- encoding=utf8 -*-
"""contains Api tests from TC"""

import json
import allure
import pytest
from requests import Response
from utils.api import Api
from utils.config_reader import ConfigReader


class TestApi:
    """class TestApi"""

    @staticmethod
    @pytest.mark.run(order=1)
    @pytest.mark.user
    @allure.feature('Test Api')
    @allure.story('Registration test')
    @allure.severity('blocker')
    def test_register():
        """Registration test"""
        with allure.step(f'Create POST request register'):
            result: Response = Api.register()
        with allure.step(f'The request has been sent, look at the response code.'):
            assert result.status_code == 200, f'Response: {result} != 200'

    @staticmethod
    @pytest.mark.run(order=2)
    @pytest.mark.user
    @allure.feature('Test Api')
    @allure.story('Login test')
    @allure.severity('blocker')
    def test_login():
        """Login test"""
        with allure.step(f'Create GET request login'):
            result: Response = Api.login()
        with allure.step(f'The request has been sent, look at the response json data'):
            response_json = result.json()
            allure.attach('response_headers', json.dumps(dict(response_json), indent=4))
            assert f'logged in user session:' in response_json['message']

    @staticmethod
    @pytest.mark.run(order=4)
    @pytest.mark.user
    @allure.feature('Test Api')
    @allure.story("Get user info test")
    @allure.severity('critical')
    def test_get_user_info():
        """Get user info test"""
        with allure.step(f'Create GET request login with username: string'):
            result: Response = Api.get_user_info()
        with allure.step(f'The request has been sent, look at the response json data.'):
            response_json = result.json()
            allure.attach('response_headers', json.dumps(dict(response_json), indent=4))
            assert ConfigReader.api_username in response_json['username']

    @staticmethod
    @pytest.mark.run(order=3)
    @pytest.mark.user
    @allure.feature('Test Api')
    @allure.story("Logout test")
    @allure.severity('critical')
    def test_logout_user():
        """Logout test"""
        with allure.step(f"Create GET logout request"):
            result: Response = Api.logout_user()
        with allure.step(f"The request has been sent, look at the response code."):
            assert result.status_code == 200, f'Response: {result} != 200'

    @staticmethod
    @pytest.mark.run(order=5)
    @pytest.mark.user
    @allure.feature('Test Api')
    @allure.story("Dell user test")
    @allure.severity('critical')
    def test_delete():
        """Dell user test"""
        with allure.step(f"Create DELL user request"):
            result: Response = Api.delete_user()
        with allure.step(f"The request has been sent, look at the response json data."):
            response_json = result.json()
            allure.attach('response_headers', json.dumps(dict(response_json), indent=4))
            assert ConfigReader.api_username in response_json['message']

    @staticmethod
    @pytest.mark.run(order=1)
    @pytest.mark.pet
    @allure.feature('Test Api')
    @allure.story("Add pet test")
    @allure.severity('blocker')
    def test_add_pet():
        """Add pet test"""
        with allure.step(f'Create POST request Add pet'):
            result: Response = Api.add_pet()
        with allure.step(f"The request has been sent, look at the response json data."):
            response_json = result.json()
            allure.attach('response_headers', json.dumps(dict(response_json), indent=4))
            assert ConfigReader.api_petname in response_json['name']

    @staticmethod
    @pytest.mark.run(order=2)
    @pytest.mark.pet
    @allure.feature('Test Api')
    @allure.story("Edit pet name test")
    @allure.severity('critical')
    def test_update_name_pet():
        """Edit pet name test"""
        with allure.step(f'Create PUT request for update pet name'):
            result: Response = Api.update_name_pet()
        with allure.step(f"The request has been sent, look at the response json data."):
            response_json = result.json()
            allure.attach('response_headers', json.dumps(dict(response_json), indent=4))
            assert ConfigReader.api_changed_pet_name in response_json['name']


if __name__ == '__main__':
    pytest.main()
