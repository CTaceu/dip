#!/usr/bin/python3
# -*- encoding=utf8 -*-
"""contains several tests for http://localhost:8000/admin/auth/user/ from TC"""

import allure
import pytest
from selenium.webdriver.support.ui import Select
from pages.auth_users_page import AuthUsersPage
from utils.config_reader import ConfigReader
from utils.postg_db import DataBase


class TestAuthUsersPage:
    """class TestAuthUsersPage"""

    @staticmethod
    @pytest.mark.run(order=5)
    @pytest.mark.tc_3
    @allure.feature('Test Auth User Page')
    @allure.story('Test if the user can logout')
    def test_logout_from_site(web_browser_login):
        """Checking if the user can logout"""
        with allure.step(f'Check if "Logged out" displayed on page source'):
            page = AuthUsersPage(web_browser_login)
            page.logout_page.click()
            assert 'Logged out' in page.get_page_source()

    @staticmethod
    @pytest.mark.run(order=6)
    @pytest.mark.tc_3
    @allure.feature('Test Auth User Page')
    @allure.story('Test if the user can login')
    def test_enter_user_login(web_browser_login_user):
        """Checking if the user can login"""
        with allure.step(f'Check if {ConfigReader.user_username} displayed on page source'):
            page = AuthUsersPage(web_browser_login_user)
            assert ConfigReader.user_username in page.get_page_source()

    @staticmethod
    @pytest.mark.run(order=3)
    @pytest.mark.tc_2
    @pytest.mark.tc_3
    @allure.feature('Test Auth Group Page')
    @allure.story('Test showed user in groups_db')
    def test_showed_user_in_groups_db(web_browser_login, setup_database):
        """Create user,check create or not and add to group"""
        with allure.step(f'Login and moved to add user page'):
            page = AuthUsersPage(web_browser_login)
            page.add_user_btn.click()
            with allure.step(f'Create user with username:'
                             f' {ConfigReader.user_username}'
                             f' and password '
                             f'{ConfigReader.user_passw}'):
                page.user_name_field.send_keys(ConfigReader.user_username)
                page.user_password_field.send_keys(ConfigReader.user_passw)
                page.user_confirm_password_field.send_keys(ConfigReader.user_passw)
                page.save_and_continue.click()
                with allure.step(f'Check if user created succesfully'
                                 f' {ConfigReader.user_username}'
                                 f' and password '
                                 f'{ConfigReader.user_passw}'):
                    assert ConfigReader.user_username in page.success_change.get_text()
            with allure.step(f'Add permission to user'):
                page.staff_user.click()
                page.super_user.click()
                page.scroll_down()
            with allure.step(f'Add user to group'):
                select = Select(page.select_group.find())
                select.select_by_visible_text(ConfigReader.group_name)
                page.add_into_group.click()
                page.scroll_down()
                page.sbmt_btn.click()
            with allure.step(f'Check if {ConfigReader.user_username} succesfully added in group'):
                assert ConfigReader.user_username in page.success_change.get_text()
            with allure.step(f'Recive {ConfigReader.user_username} ID in DB'):
                user_id = DataBase(setup_database).show_data_in_tables_users_from_username()[0]
        with allure.step(f'Check if {ConfigReader.user_username} is in the DB'):
            assert user_id in DataBase(setup_database).show_data_in_group_users()


if __name__ == '__main__':
    pytest.main()
