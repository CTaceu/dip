#!/usr/bin/python3
# -*- encoding=utf8 -*-

"""fixtures for future tests """

import pytest


# мб добавлю тестов кроме TC

@pytest.fixture(scope="function", params=[('admin', 'passw00rd'),
                                          ('adm11n', 'password'),
                                          ('adm11n', 'passw00rd')])
def param_test(request):
    """ fixture generate uncorrect data for login """
    return request.param


@pytest.fixture(scope="function", params=[('admin', ''),
                                          ('', 'password'),
                                          ('', '')])
def param_test_one_arg(request):
    """ fixture generate uncorrect data for login """
    return request.param
