#!/usr/bin/python3
# -*- encoding=utf8 -*-
"""contains several tests for http://localhost:8000/admin/login/?next=/admin/ from TC"""

import allure
import pytest
from pages.auth_page import AuthPage
from utils.config_reader import ConfigReader


class TestAuthPage:
    """class TestAuthPage"""

    @staticmethod
    @pytest.mark.run(order=2)
    @pytest.mark.tc_1
    @pytest.mark.tc_2
    @pytest.mark.tc_3
    @pytest.mark.tc_4
    @allure.feature('Test Auth Page')
    @allure.story('Test authorisation on the site')
    def test_authorisation(web_browser):
        """Open site and check authorisation with correct data"""
        with allure.step(f'Open auth page'):
            page = AuthPage(web_browser)
        with allure.step(f'Login with {ConfigReader.admin_name}: {ConfigReader.adm_passw}'):
            page.email.send_keys(ConfigReader.admin_name)
            page.password.send_keys(ConfigReader.adm_passw)
            page.btn_login.click()
        with allure.step(f'Check  {ConfigReader.admin_name}: {ConfigReader.adm_passw}'):
            assert page.get_current_url() == ConfigReader.success_page

    @staticmethod
    @pytest.mark.run(order=1)
    @pytest.mark.uncorrect_data
    def test_check_authorisation_with_uncorrect_data(web_browser, param_test):
        """Open site and check authorisation with uncorrect data"""
        with allure.step(f'Open auth page'):
            page = AuthPage(web_browser)
            (login, password) = param_test
        with allure.step(f'Login with {login}: {password}'):
            page.email.send_keys(login)
            page.password.send_keys(password)
            page.btn_login.click()
        with allure.step(f'Check  {login}: {password}'):
            assert page.err_wind.get_text() == 'Please enter the correct username' \
                                               ' and password for a staff account. ' \
                                               'Note that both fields may be case-sensitive.'

    @staticmethod
    @pytest.mark.run(order=1)
    @pytest.mark.uncorrect_data
    def test_check_authorisation_with_one_arg(web_browser, param_test_one_arg):
        """Open site and check authorisation with uncorrect data (one field is empty)"""
        with allure.step(f'Open auth page'):
            page = AuthPage(web_browser)
            (login, password) = param_test_one_arg
        with allure.step(f'Login with {login}: {password}'):
            page.email.send_keys(login)
            page.password.send_keys(password)
            page.btn_login.click()
        with allure.step(f'Check  {login}: {password}'):
            assert page.get_current_url() == ConfigReader.not_logged_in


if __name__ == '__main__':
    pytest.main()
