#!/usr/bin/python3
# -*- encoding=utf8 -*-
"""contains DB tests from TC"""

import allure
import pytest
from utils.config_reader import ConfigReader
from utils.postg_db import DataBase


class TestPostgre:
    """class TestPostgre"""

    @staticmethod
    @pytest.mark.run(order=1)
    @pytest.mark.tc_1
    @pytest.mark.tc_2
    @pytest.mark.tc_3
    @allure.feature('Test Postgre DB')
    @allure.story(f'Create group and check group name "{ConfigReader.group_name}" in DB')
    def test_show_in_db(setup_database):
        """Create group and check group name in DB"""
        database = DataBase(setup_database)
        with allure.step(f'Create table with table_name'):
            database.insert_into_table()
            assert (f'{ConfigReader.group_name}',) in database.show_column_name_in_group_tables()

    @staticmethod
    @pytest.mark.run(order=4)
    @pytest.mark.tc_3
    @allure.feature('Test Postgre DB')
    @allure.story('Check if user successful created in DB')
    def test_user_in_db(setup_database):
        """Check if user successfully created in db"""
        database = DataBase(setup_database)
        with allure.step(f'Create user with username "{ConfigReader.user_username}" '):
            user_id = database.show_data_in_tables_users_from_username()[0]
            assert user_id in database.show_data_in_tables_users()


if __name__ == '__main__':
    pytest.main()
