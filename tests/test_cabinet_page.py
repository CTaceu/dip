#!/usr/bin/python3
# -*- encoding=utf8 -*-
"""contains several tests for http://localhost:8000/admin/login/?next=/admin/ from TC"""

import allure
import pytest
from pages.cabinet_page import CabinetPage
from utils.config_reader import ConfigReader


class TestCabinetPage:
    """class TestLoginPage"""

    @staticmethod
    @pytest.mark.run(order=3)
    @pytest.mark.tc_4
    @allure.feature('Test Cabinet Page')
    @allure.story("Remove first pic from site")
    def test_dell_first_pic(web_browser_login):
        """Remove first pic from site"""
        with allure.step(f"Login"):
            page = CabinetPage(web_browser_login)
        with allure.step(f"Move to main page and generate pic"):
            page.view_site_page.click()
            page.go_back()
            page.edit_picture.click()
            page.scroll_down()
            attr_pic = page.all_pic_count.get_attribute('value')[-1]
        with allure.step(f"Collect all pic with att 'value' and select last with att {attr_pic}."):
            page.get(f'{ConfigReader.url_post_page}{attr_pic}')
        with allure.step(f"Remove selected pic "):
            page.del_pic.click()
            page.del_pic_yes.click()
        with allure.step(f"Checking for a successful deletion message"):
            assert page.del_pic_success.get_text() == \
                   f'The post “Post object ({attr_pic})” was deleted successfully.'


if __name__ == '__main__':
    pytest.main()
