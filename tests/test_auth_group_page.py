#!/usr/bin/python3
# -*- encoding=utf8 -*-
"""contains several tests for http://localhost:8000/admin/auth/group/ from TC"""

import allure
import pytest
from pages.auth_group_page import AuthGroupPage
from utils.config_reader import ConfigReader


class TestAuthGroupPage:
    """class TestAuthGroupPage"""

    @staticmethod
    @pytest.mark.run(order=3)
    @pytest.mark.tc_1
    @allure.feature('Test Auth Group Page')
    @allure.story('Checking if the group we created exists')
    def test_showed_groups(web_browser_login):
        """Checking if the group we created exists"""
        with allure.step(f'Login and moved to {ConfigReader.auth_url_get}'):
            page = AuthGroupPage(web_browser_login)
        with allure.step(f'Checking if group {ConfigReader.group_name} exists'):
            assert ConfigReader.group_name in page.group_elem.get_text()[0][::],\
                f'Group with name: {ConfigReader.group_name} not found '


if __name__ == '__main__':
    pytest.main()
