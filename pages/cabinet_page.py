#!/usr/bin/python3
# -*- encoding=utf8 -*-
"""Cabinet Page class"""

from pages.base import WebPage
from pages.elements import WebElement, ManyWebElements
from utils.config_reader import ConfigReader


class CabinetPage(WebPage):
    """Cabinet page class """

    def __init__(self, web_driver, url=''):
        url = ConfigReader.cabinet_url_get
        super().__init__(web_driver, url)

    view_site_page = WebElement(css_selector='#user-tools > a:nth-child(2)')
    first_picture = WebElement(css_selector='body > main > div > div > div > div:nth-child(1)')
    edit_picture = WebElement(
        css_selector='#content-main > div.app-app.module > table > tbody > tr > td:nth-child(3) > a'
    )
    all_pic_count = ManyWebElements(name='_selected_action')
    del_pic = WebElement(class_name='deletelink')
    del_pic_yes = WebElement(xpath='//*[@id="content"]/form/div/input[2]')
    del_pic_success = WebElement(class_name='success')
