#!/usr/bin/python3
# -*- encoding=utf8 -*-
"""AuthGroupPage page class"""
from pages.base import WebPage
from pages.elements import WebElement, ManyWebElements
from utils.config_reader import ConfigReader


class AuthGroupPage(WebPage):
    """Class for /admin/auth/group/ page"""
    def __init__(self, web_driver, url=''):
        url = ConfigReader.auth_url_get
        super().__init__(web_driver, url)

    cabinet_page = WebElement(css_selector='#site-name > a:nth-child(1)')
    view_site_page = WebElement(css_selector='#user-tools > a:nth-child(2)')
    change_pass_page = WebElement(css_selector='#user-tools > a:nth-child(3)')
    logout_page = WebElement(css_selector='#user-tools > a:nth-child(4)')
    home_page = WebElement(xpath='//*[@id="container"]/div[2]/a[1]')
    auth_page = WebElement(xpath='//*[@id="container"]/div[2]/a[2]')

    search_filter = WebElement(id='nav-filter')
    search_bar = WebElement(id='searchbar')
    search_btn = WebElement(css_selector='#changelist-search > div > input[type=submit]:nth-child(3)')

    selector_field = WebElement(css_selector='#changelist-form > div.actions > label > select')
    selector_btn = WebElement(css_selector='#changelist-form > div.actions > button')
    checkbox_all = WebElement(id='action-toggle')
    group_elem = ManyWebElements(xpath='//*[@id="result_list"]/tbody')
