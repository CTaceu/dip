#!/usr/bin/python3
# -*- encoding=utf8 -*-
"""Base Page class"""
from termcolor import colored


class WebPage:
    """Base Page class"""

    def __init__(self, web_driver, url=''):
        self._web_driver = web_driver
        self.get(url)

    def __getattribute__(self, item):   # вызывается первым, получаем вебдрайвер
        attr = object.__getattribute__(self, item)

        if not item.startswith('_') and not callable(attr):
            attr._web_driver = self._web_driver
        return attr

    def get(self, url):
        """get page"""
        self._web_driver.get(url)

    def go_back(self):
        """go back to page"""
        self._web_driver.back()

    def refresh(self):
        """refresh page"""
        self._web_driver.refresh()

    def screenshot(self, file_name='screenshot.png'):
        """make screenshot"""
        self._web_driver.save_screenshot(file_name)

    def scroll_down(self, offset=0):
        """ Scroll the page down. """

        if offset:
            self._web_driver.execute_script(f'window.scrollTo(0, {offset});')
        else:
            self._web_driver.execute_script('window.scrollTo(0, document.body.scrollHeight);')

    def scroll_up(self, offset=0):
        """ Scroll the page up. """

        if offset:
            self._web_driver.execute_script(f'window.scrollTo(0, -{offset});')
        else:
            self._web_driver.execute_script('window.scrollTo(0, -document.body.scrollHeight);')

    def switch_to_iframe(self, iframe):
        """ Switch to iframe by it's name. """
        self._web_driver.switch_to.frame(iframe)

    def switch_out_iframe(self):
        """ Cancel iframe focus. """
        self._web_driver.switch_to.default_content()

    def get_current_url(self):
        """ Returns current browser URL. """
        return self._web_driver.current_url

    def get_page_source(self):
        """ Returns current page body. """

        source = ''
        try:
            source = self._web_driver.page_source
        except:
            print(colored('Can not get page source', 'red'))

        return source

    def check_js_errors(self, ignore_list=None):
        """ This function checks JS errors on the page. """

        ignore_list = ignore_list or []

        logs = self._web_driver.get_log('browser')
        for log_message in logs:
            if log_message['level'] != 'WARNING':
                ignore = False
                for issue in ignore_list:
                    if issue in log_message['message']:
                        ignore = True
                        break

                assert ignore, f'JS error "{log_message}" on the page!'
