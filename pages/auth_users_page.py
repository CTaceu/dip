#!/usr/bin/python3
# -*- encoding=utf8 -*-
"""AuthUsers page class"""
from pages.base import WebPage
from pages.elements import WebElement
from utils.config_reader import ConfigReader


class AuthUsersPage(WebPage):
    """AuthUsers page class """

    def __init__(self, web_driver, url=''):
        url = ConfigReader.auth_users_page_get
        super().__init__(web_driver, url)

    cabinet_page = WebElement(css_selector='#site-name > a:nth-child(1)')
    view_site_page = WebElement(css_selector='#user-tools > a:nth-child(2)')
    change_pass_page = WebElement(css_selector='#user-tools > a:nth-child(3)')
    logout_page = WebElement(css_selector='#user-tools > a:nth-child(4)')
    home_page = WebElement(
        xpath='//*[@id="container"]/div[2]/a[1]')
    auth_page = WebElement(
        xpath='//*[@id="container"]/div[2]/a[2]')

    search_filter = WebElement(
        id='nav-filter')
    search_bar = WebElement(
        id='searchbar')
    add_user_btn = WebElement(xpath='//*[@id="nav-sidebar"]/div[2]/table/tbody/tr[2]/td/a')

    user_name_field = WebElement(id='id_username')
    user_password_field = WebElement(id='id_password1')
    user_confirm_password_field = WebElement(id='id_password2')
    save_and_continue = WebElement(name='_continue')

    search_btn = WebElement(css_selector='#changelist-search > div > input[type=submit]:nth-child(3)')
    selector_field = WebElement(css_selector='#changelist-form > div.actions > label > select')
    selector_btn = WebElement(css_selector='#changelist-form > div.actions > button')

    checkbox_all = WebElement(id='action-toggle')
    select_group = WebElement(id='id_groups_from')
    add_into_group = WebElement(xpath='//*[@id="id_groups_add_link"]')
    sbmt_btn = WebElement(xpath='//*[@id="user_form"]/div/div/input[1]')

    fields_username = WebElement(class_name='field-username')
    success_change = WebElement(css_selector='#main > div > ul > li')

    staff_user = WebElement(id='id_is_staff')
    super_user = WebElement(id='id_is_superuser')

    filter_staff = WebElement(xpath='//*[@id="changelist-filter"]/ul[1]/li[1]/a ')
    filter_staff_yes = WebElement(xpath='//*[@id="changelist-filter"]/ul[1]/li[2]/a ')
    filter_staff_no = WebElement(xpath='//*[@id="changelist-filter"]/ul[1]/li[3]/a ')

    filter_super_user = WebElement(xpath='//*[@id="changelist-filter"]/ul[2]/li[1]/a')
    filter_super_user_yes = WebElement(xpath='//*[@id="changelist-filter"]/ul[2]/li[2]/a ')
    filter_super_user_no = WebElement(xpath='//*[@id="changelist-filter"]/ul[2]/li[3]/a ')

    filter_active = WebElement(xpath='//*[@id="changelist-filter"]/ul[3]/li[1]/a')
    filter_active_yes = WebElement(xpath='//*[@id="changelist-filter"]/ul[3]/li[2]/a ')
    filter_active_no = WebElement(xpath='//*[@id="changelist-filter"]/ul[3]/li[3]/a ')
    clear_all_filters = WebElement(xpath='//*[@id="changelist-filter-clear"]/a')
