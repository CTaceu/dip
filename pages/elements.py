#!/usr/bin/python3
# -*- encoding=utf8 -*-
"""helper class to define web elements on web pages"""

import time
import logging
from termcolor import colored
from selenium.webdriver import ActionChains
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys


class WebElement:
    """Base element class"""
    _locator = ('', '')
    _web_driver = None
    _timeout = 7
    LOGGER = logging.getLogger(__name__)

    def __init__(self, timeout=10, **kwargs):
        self._timeout = timeout

        for attr in kwargs:
            self._locator = (str(attr).replace('_', ' '), str(kwargs.get(attr)))

    def find(self, timeout=7):
        """ Find element on the page. """

        element = None

        try:
            element = WebDriverWait(self._web_driver, timeout).until(
                EC.presence_of_element_located(self._locator)
            )
        except:
            print(colored('Element not found on the page!', 'red'))
        WebElement.LOGGER.info(
            'Trying to find element on the page {0}'.format(self._locator)
        )
        return element

    def wait_to_be_clickable(self, timeout=10, check_visibility=True):
        """ Wait until the element will be ready for click. """

        element = None

        try:
            element = WebDriverWait(self._web_driver, timeout).until(
                EC.element_to_be_clickable(self._locator)
            )
        except:
            print(colored('Element not clickable!', 'red'))

        if check_visibility:
            self.wait_until_not_visible()
        WebElement.LOGGER.info('Wait for clickable element {0}.'.format(self._locator))
        return element

    def is_clickable(self):
        """ Check is element ready for click or not. """

        element = self.wait_to_be_clickable(timeout=1)
        WebElement.LOGGER.info(
            'Check is element {0} ready for click or not.'.format(self._locator)
        )
        return element is not None

    def is_presented(self):
        """ Check that element is presented on the page. """

        element = self.find(timeout=1)
        WebElement.LOGGER.info(
            'Check that element {0} is presented on the page.'.format(self._locator)
        )
        return element is not None

    def is_visible(self):
        """ Check is the element visible or not. """

        element = self.find(timeout=1)

        if element:
            return element.is_displayed()
        WebElement.LOGGER.info('Check that element {0} visible or not.'.format(self._locator))
        return False

    def wait_until_not_visible(self, timeout=10):

        element = None

        try:
            element = WebDriverWait(self._web_driver, timeout).until(
                EC.visibility_of_element_located(self._locator)
            )
        except:
            print(colored('Element not visible!', 'red'))

        if element:
            js = ('return (!(arguments[0].offsetParent === null) && '
                  '!(window.getComputedStyle(arguments[0]) === "none") &&'
                  'arguments[0].offsetWidth > 0 && arguments[0].offsetHeight > 0'
                  ');')
            visibility = self._web_driver.execute_script(js, element)
            iteration = 0

            while not visibility and iteration < 10:
                time.sleep(0.5)

                iteration += 1

                visibility = self._web_driver.execute_script(js, element)
                print('Element {0} visibility: {1}'.format(self._locator, visibility))

        return element

    def send_keys(self, keys, wait=0.5):
        """ Send keys to the element. """

        keys = keys.replace('\n', '\ue007')

        element = self.find()

        if element:
            element.click()
            element.clear()
            element.send_keys(keys)
            time.sleep(wait)
        else:
            msg = 'Element with locator {0} not found'
            raise AttributeError(msg.format(self._locator))
        WebElement.LOGGER.info('Send keys "{1}" to the element {0}.'.format(self._locator, keys))

    def get_text(self):
        """ Get text of the element. """

        element = self.find()
        text = ''

        try:
            text = str(element.text)
        except Exception as e:
            print('Error: {0}'.format(e))
        WebElement.LOGGER.info(
            'Get text {1} of the element {0}.'.format(self._locator, text)
        )
        return text

    def get_attribute(self, attr_name):
        """ Get attribute of the element. """

        element = self.find()

        if element:
            return element.get_attribute(attr_name)
        WebElement.LOGGER.info(
            'Get attribute {1} of the element {0}.'.format(self._locator, attr_name)
        )

    def click(self, hold_seconds=0, x_offset=1, y_offset=1):
        """ Wait and click the element. """

        element = self.wait_to_be_clickable()

        if element:
            action = ActionChains(self._web_driver)
            action.move_to_element_with_offset(element, x_offset, y_offset). \
                pause(hold_seconds).click(on_element=element).perform()
        else:
            msg = 'Element with locator {0} not found'
            raise AttributeError(msg.format(self._locator))

        WebElement.LOGGER.info('Wait and click the element {0}.'.format(self._locator))

    def right_mouse_click(self, x_offset=0, y_offset=0, hold_seconds=0):
        """ Click right mouse button on the element. """

        element = self.wait_to_be_clickable()

        if element:
            action = ActionChains(self._web_driver)
            action.move_to_element_with_offset(element, x_offset, y_offset). \
                pause(hold_seconds).context_click(on_element=element).perform()
        else:
            msg = 'Element with locator {0} not found'
            raise AttributeError(msg.format(self._locator))
        WebElement.LOGGER.info('Click right mouse button on the element {0}.'.format(self._locator))

    def highlight_and_make_screenshot(self, file_name='element.png'):
        """ Highlight element and make the screen-shot of all page. """

        element = self.find()

        # Scroll page to the element:
        self._web_driver.execute_script("arguments[0].scrollIntoView();", element)

        # Add red border to the style:
        self._web_driver.execute_script("arguments[0].style.border='3px solid red'", element)

        # Make screen-shot of the page:
        self._web_driver.save_screenshot(file_name)
        WebElement.LOGGER.info(
            'Highlight element and make the screen-shot {0} of all page.'.format(file_name)
        )

    def scroll_to_element(self):
        """ Scroll page to the element. """

        element = self.find()
        try:
            element.send_keys(Keys.DOWN)
        except Exception as e:
            pass  # Just ignore the error if we can't send the keys to the element


class ManyWebElements(WebElement):

    def __getitem__(self, item):
        """ Get list of elements and try to return required element. """

        elements = self.find()
        return elements[item]

    def find(self, timeout=10):
        """ Find elements on the page. """

        elements = []

        try:
            elements = WebDriverWait(self._web_driver, timeout).until(
                EC.presence_of_all_elements_located(self._locator)
            )
        except:
            print(colored('Elements not found on the page!', 'red'))
        WebElement.LOGGER.info(
            'Get list of elements and try to return required element {0}.'.format(self._locator)
        )
        return elements

    def click(self, hold_seconds=0, x_offset=0, y_offset=0):
        """ Note: this action is not applicable for the list of elements. """
        WebElement.LOGGER.info(
            'This action is not applicable for the list of elements {0}.'.format(self._locator)
        )
        raise NotImplemented('This action is not applicable for the list of elements')

    def count(self):
        """ Get count of elements. """

        elements = self.find()
        WebElement.LOGGER.info('Get count of elements {0}.'.format(self._locator))
        return len(elements)

    def get_text(self):
        """ Get text of elements. """

        global text
        elements = self.find()
        result = []

        for element in elements:
            text = ''
            try:
                text = element.text.split('\n')
            except Exception as e:
                print('Error: {0}'.format(e))

            result.append(text)
        WebElement.LOGGER.info(
            'Get text of elements {0}.'.format(self._locator)
        )
        return result

    def get_attribute(self, attr_name):
        """ Get attribute of all elements. """

        results = []
        elements = self.find()

        for element in elements:
            results.append(element.get_attribute(attr_name))
        WebElement.LOGGER.info(
            'Get attribute of all elements {0}.'.format(self._locator)
        )
        return results

    def highlight_and_make_screenshot(self, file_name='element.png'):
        """ Highlight elements and make the screen-shot of all page. """

        elements = self.find()

        for element in elements:
            # Scroll page to the element:
            self._web_driver.execute_script("arguments[0].scrollIntoView();", element)

            # Add red border to the style:
            self._web_driver.execute_script("arguments[0].style.border='3px solid red'", element)

        # Make screen-shot of the page:
        self._web_driver.save_screenshot(file_name)
        WebElement.LOGGER.info(
            'Highlight elements and make the screen-shot {0} of all page.'.format(file_name)
        )
