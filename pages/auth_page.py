#!/usr/bin/python3
# -*- encoding=utf8 -*-
"""AuthPage page class"""
from pages.base import WebPage
from pages.elements import WebElement, ManyWebElements
from utils.config_reader import ConfigReader


class AuthPage(WebPage):
    """Auth Page Class"""
    def __init__(self, web_driver, url=''):
        url = ConfigReader.cabinet_url_get
        super().__init__(web_driver, url)

    email = WebElement(id='id_username')
    mail = WebElement(css_selector='#login-form > div:nth-child(2)')
    password = WebElement(id='id_password')
    btn_login = WebElement(css_selector='.submit-row > input:nth-child(1)')
    err_wind = WebElement(css_selector='.errornote')
    all_elem = ManyWebElements(xpath=' //*[@id="login-form"]/div')
