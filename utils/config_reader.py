#!/usr/bin/python3
# -*- encoding=utf8 -*-
"""contains helper class for reading config file"""

import configparser
import os
from pathlib import Path


class ConfigReader:
    """Class config reader for simple_config.ini file"""

    parser = configparser.ConfigParser()
    path = Path(__file__)
    root_dir = path.parent.absolute()
    config_path = os.path.join(root_dir, "../utils/simple_config.ini")
    parser.read(config_path)
    # conftest data block
    conftest_url = parser.get('url_conf', 'url')
    conftest_adm_name = parser.get('url_conf', 'admin_name')
    conftest_adm_passw = parser.get('url_conf', 'admin_pass')
    conftest_user_name = parser.get('url_conf', 'user_username')
    conftest_user_password = parser.get('url_conf', 'user_passw')
    # Page data block
    base_url = parser.get('base_url', 'url')
    auth_url_get = base_url + parser.get('auth_group_page_data', 'url')
    cabinet_url_get = base_url + parser.get('cabinet_page_data', 'url')
    auth_users_page_get = base_url + parser.get('auth_users_page_data', 'url')
    url_post_page = base_url + parser.get('cabinet_page_data', 'url_post_page')
    success_page = base_url + parser.get('auth_page_data', 'success_login_page')
    not_logged_in = base_url + parser.get('auth_page_data', 'url')
    admin_name = parser.get('auth_page_data', 'admin_name')
    adm_passw = parser.get('auth_page_data', 'admin_pass')
    # PostgreSQL block
    user_username = parser.get('postgre_data', 'user_username')
    user_passw = parser.get('postgre_data', 'user_passw')
    group_id = parser.get('postgre_data', 'group_id')
    group_name = parser.get('postgre_data', 'group_name')
    dbname = parser.get('postgre_data', 'dbname')
    user = parser.get('postgre_data', 'user')
    password = parser.get('postgre_data', 'password')
    host = parser.get('postgre_data', 'host')
    # Api Data block
    base_api_url = parser.get('urls', 'url')
    info_url = parser.get('urls', 'get_info')
    dell_url = parser.get('urls', 'dell')
    register = base_api_url + parser.get('urls', 'register')
    login = base_api_url + parser.get('urls', 'login')
    login_part = parser.get('urls', 'login_part')
    login_name = parser.get('urls', 'login_name')
    login_passw = parser.get('urls', 'login_passw')
    login_link = login + login_name + login_part + login_passw
    get_info = base_api_url + info_url + parser.get('users_data', 'login')
    do_logout = base_api_url + parser.get('urls', 'logout')
    dell_some = base_api_url + dell_url + parser.get('users_data', 'login')
    pet_url = parser.get('urls_pet', 'url_pet')
    add_data = pet_url + parser.get('pet_data', 'add_pet')
    api_username = parser.get('users_data', 'login')
    api_petname = parser.get('pet_data', 'pet_name')
    api_changed_pet_name = parser.get('pet_data', 'pet_changed_name')
