import logging
from utils.config_reader import ConfigReader


class DataBase:
    """class for Database"""
    _base_driver = None

    def __init__(self, base_driver):
        self._base_driver = base_driver
    LOGGER = logging.getLogger(__name__)

    def show_column_name_in_group_tables(self):
        """show column name in auth_group"""
        conn = self._base_driver
        with conn.cursor() as cursor:
            cursor.execute("SELECT name FROM auth_group")
            DataBase.LOGGER.info(
                'SELECT name FROM auth_group'
            )
            return cursor.fetchall()

    def show_data_in_tables_users(self):
        """show id in auth_user"""
        conn = self._base_driver
        with conn.cursor() as cursor:
            cursor.execute("SELECT id  FROM auth_user")
            DataBase.LOGGER.info(
                'SELECT id  FROM auth_user'
            )
            return cursor.fetchall()

    def show_data_in_tables_users_from_username(self):
        """show id in auth_user"""
        conn = self._base_driver
        with conn.cursor() as cursor:
            cursor.execute(f"SELECT id  FROM auth_user WHERE username='"
                           f"{ConfigReader.user_username}'")
            DataBase.LOGGER.info(
                'SELECT id  FROM auth_user WHERE username= {0}'.format(ConfigReader.user_username)
            )
            return cursor.fetchall()

    def insert_into_table(self):
        """create table in auth_group"""
        conn = self._base_driver
        with conn.cursor() as cursor:
            cursor.execute("INSERT INTO auth_group(id, name) VALUES (%s, %s)",
                           (f'{ConfigReader.group_id}', f'{ConfigReader.group_name}'))
            DataBase.LOGGER.info(
                'INSERT INTO auth_group(id, name) VALUES {0}, {1}'.format(ConfigReader.group_id,
                                                                          ConfigReader.group_name))
            conn.commit()

    def dell_group_from_db(self):
        """delete group by name from auth_group"""
        conn = self._base_driver
        with conn.cursor() as cursor:
            select_orders_beetwen_query = f'''
                        DELETE  FROM auth_group WHERE name='{ConfigReader.group_name}';
                        '''
            cursor.execute(select_orders_beetwen_query)
            DataBase.LOGGER.info(
                'DELETE  FROM auth_group WHERE name="{0}"'.format(ConfigReader.group_name)
            )
            conn.commit()

    def dell_user_group_from_db(self):
        """delete user from auth_users_groups by group_id"""
        conn = self._base_driver
        with conn.cursor() as cursor:
            select_orders_beetwen_query = f'''
                        DELETE FROM auth_user_groups WHERE group_id='{ConfigReader.group_id}';
                        '''
            cursor.execute(select_orders_beetwen_query)
            conn.commit()

    def dell_user_from_db(self):
        """delete user from auth_user by username"""
        conn = self._base_driver
        with conn.cursor() as cursor:
            select_orders_between_query = f'''
                        DELETE FROM auth_user WHERE username='{ConfigReader.user_username}';
                        '''
            cursor.execute(select_orders_between_query)
            conn.commit()

    def show_data_in_group_users(self):
        """show user_id from auth_user_groups by group_id"""
        conn = self._base_driver
        with conn.cursor() as cursor:
            cursor.execute(f"SELECT user_id "
                           f"FROM auth_user_groups "
                           f"WHERE group_id='{ConfigReader.group_id}' ")
            return cursor.fetchall()

    def show_data_in_tables_group(self):
        """show all data from auth_group"""
        conn = self._base_driver
        with conn.cursor() as cursor:
            cursor.execute('SELECT * '
                           'FROM auth_group')
            print('\n', cursor.fetchall())

    def show_tables_name_in_db(self):
        """show table_name"""
        conn = self._base_driver
        with conn.cursor() as cursor:
            cursor.execute("SELECT table_name "
                           "FROM information_schema.tables "
                           "WHERE table_schema='public' "
                           "ORDER BY table_name")
            print('\n', cursor.fetchall())

    def show_data_in_tables_posts(self):
        """show column_name in  table app_post"""
        conn = self._base_driver
        with conn.cursor() as cursor:
            cursor.execute("SELECT columns.column_name "
                           "FROM information_schema.columns  "
                           "WHERE table_name='app_post'")
            print('\n', cursor.fetchall())

    def show_data_in_id_tables_posts(self):
        """show id from app_post table"""
        conn = self._base_driver
        with conn.cursor() as cursor:
            cursor.execute("SELECT id FROM app_post")
            print('\n', cursor.fetchall())

    def show_tables_id_in_users(self):
        """show column_name in auth_user table"""
        conn = self._base_driver
        with conn.cursor() as cursor:
            cursor.execute("SELECT columns.column_name "
                           "FROM information_schema.columns  "
                           "WHERE table_name='auth_user'")
            print('\n', cursor.fetchall())

    def show_column_in_group_users(self):
        """show column_name in auth_user_groups table"""
        conn = self._base_driver
        with conn.cursor() as cursor:
            cursor.execute(
                "SELECT columns.column_name "
                "FROM information_schema.columns  "
                "WHERE table_name='auth_user_groups'")
            print('\n', cursor.fetchall())

    def show_column_in_group(self):
        """show column_name in auth_group table"""
        conn = self._base_driver
        with conn.cursor() as cursor:
            cursor.execute("SELECT columns.column_name "
                           "FROM information_schema.columns  "
                           "WHERE table_name='auth_group'")
            print('\n', cursor.fetchall())
