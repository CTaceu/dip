#!/usr/bin/python3
# -*- encoding=utf8 -*-
"""contains helper Api class for work with api"""

import os
import logging
import json
from pathlib import Path
import requests
from utils.config_reader import ConfigReader


class Api:
    """class API"""
    path = Path(__file__)
    root_dir = path.parent.absolute()
    config_path = os.path.join(root_dir, "../utils/api_data.json")
    LOGGER = logging.getLogger(__name__)

    @staticmethod
    def register():
        """Check registration, request body is read from /api_data.json"""
        with open(Api.config_path, 'r') as datafile:
            login_data = json.load(datafile)[0]
            url = ConfigReader.register
            json_data = login_data
            response = requests.post(url, json=login_data)
            Api.LOGGER.info('TEST: Register with %s, status %d', json_data, response.status_code)
            return response

    @staticmethod
    def login():
        """Check login"""
        login_link = ConfigReader.login_link
        response = login_link
        response = requests.get(response)
        Api.LOGGER.info('TEST: Login with %s, status %d', login_link, response.status_code)
        return response

    @staticmethod
    def get_user_info():
        """Get user info"""
        url = ConfigReader.get_info
        response = requests.get(url)
        Api.LOGGER.info('TEST: Get user info with %s, status %d', url, response.status_code)
        return response

    @staticmethod
    def logout_user():
        """Check logout"""
        url = ConfigReader.do_logout
        response = requests.get(url)
        Api.LOGGER.info('TEST: Logout with %s, status %d', url, response.status_code)
        return response

    @staticmethod
    def delete_user():
        """Check delete user"""
        url = ConfigReader.dell_some
        response = requests.delete(url)
        Api.LOGGER.info('TEST: Dell with %s, status %d', url, response.status_code)
        return response

    @staticmethod
    def add_pet():
        """Check add pet by name, request body is read from utils/api_data.json"""
        with open(Api.config_path, 'r') as datafile:
            add_pet_data = json.load(datafile)[1]
            url = ConfigReader.add_data
            response = requests.post(url, json=add_pet_data)
            Api.LOGGER.info('TEST: Add pet with %s, status %d', url, response.status_code)
            return response

    @staticmethod
    def update_name_pet():
        """Check update pet name, request body is read from utils/api_data.json"""
        with open(Api.config_path, 'r') as datafile:
            upd_pet_data = json.load(datafile)[2]
            url = ConfigReader.add_data
            response = requests.put(url, json=upd_pet_data)
            Api.LOGGER.info('TEST: Update pet name with %s, status %d', url, response.status_code)
            return response
