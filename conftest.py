#!/usr/bin/python3
# -*- encoding=utf8 -*-
import pytest
import warnings
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from webdriver_manager.chrome import ChromeDriverManager
from pages.auth_page import AuthPage
from utils.config_reader import ConfigReader
from psycopg2 import connect


def pytest_addoption(parser):
    """ in future: can choose browser"""
    parser.addoption('--browser', action='store', default="chrome",
                     help="Choose browser")


@pytest.fixture(scope='session')
def setup_database():
    """ Fixture to set up the database with test data """

    with connect(dbname=ConfigReader.dbname, user=ConfigReader.user,
                 password=ConfigReader.password, host=ConfigReader.host) as conn:
        conn.autocommit = True
        yield conn
        with conn.cursor() as cursor:
            cursor.execute(f'''DELETE FROM auth_user_groups WHERE group_id='{ConfigReader.group_id}';''')
            cursor.execute(f'''DELETE FROM auth_user WHERE username='{ConfigReader.user_username}';''')
            cursor.execute(f'''DELETE  FROM auth_group WHERE name='{ConfigReader.group_name}';''')


@pytest.fixture(scope='session')
def web_browser():
    """browser fixture"""
    chrome_options = Options()
    chrome_options.add_argument('--headless')
    chrome_options.add_argument('--no-sandbox')
    warnings.filterwarnings("ignore", category=DeprecationWarning)
    browser = webdriver.Chrome(executable_path=ChromeDriverManager().install(), options=chrome_options)
    browser.set_window_size(1920, 1080)
    browser.implicitly_wait(2)

    yield browser
    browser.quit()


@pytest.fixture(scope='session')
def web_browser_login():
    """browser fixture with admin login"""
    chrome_options = Options()
    chrome_options.add_argument('--headless')
    chrome_options.add_argument('--no-sandbox')
    warnings.filterwarnings("ignore", category=DeprecationWarning)
    browser = webdriver.Chrome(executable_path=ChromeDriverManager().install(), options=chrome_options)
    browser.implicitly_wait(2)
    browser.set_window_size(1920, 1080)
    browser.get(ConfigReader.conftest_url)
    page = AuthPage(browser)
    page.email.send_keys(ConfigReader.conftest_adm_name)
    page.password.send_keys(ConfigReader.conftest_adm_passw)
    page.btn_login.click()

    yield browser
    browser.quit()


@pytest.fixture(scope='session')
def web_browser_login_user():
    """browser fixture with user login"""
    chrome_options = Options()
    chrome_options.add_argument('--headless')
    chrome_options.add_argument('--no-sandbox')
    warnings.filterwarnings("ignore", category=DeprecationWarning)
    browser = webdriver.Chrome(executable_path=ChromeDriverManager().install(), options=chrome_options)
    browser.implicitly_wait(2)
    browser.set_window_size(1920, 1080)
    browser.get(ConfigReader.conftest_url)
    page = AuthPage(browser)
    page.email.send_keys(ConfigReader.conftest_user_name)
    page.password.send_keys(ConfigReader.conftest_user_password)
    page.btn_login.click()

    yield browser
    browser.quit()
